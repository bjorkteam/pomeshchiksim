object Form3: TForm3
  Left = 829
  Top = 126
  Width = 454
  Height = 480
  Caption = #1043#1086#1088#1086#1076' N'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lblMarketName: TLabel
    Left = 192
    Top = 8
    Width = 53
    Height = 23
    Caption = #1056#1099#1085#1086#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblUniqueBuildings: TLabel
    Left = 120
    Top = 280
    Width = 204
    Height = 23
    Caption = #1059#1085#1080#1082#1072#1083#1100#1085#1099#1077' '#1087#1086#1089#1090#1088#1086#1081#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object btnSellFur: TButton
    Left = 8
    Top = 40
    Width = 209
    Height = 33
    Caption = #1055#1088#1086#1076#1072#1090#1100' '#1086#1076#1085#1091' '#1077#1076#1080#1085#1080#1094#1091' '#1084#1077#1093#1072' (+2 '#1088#1091#1073')'
    TabOrder = 0
    OnClick = btnSellFurClick
  end
  object btnSell5Fur: TButton
    Left = 8
    Top = 80
    Width = 209
    Height = 33
    Caption = #1055#1088#1086#1076#1072#1090#1100' 5 '#1077#1076#1080#1085#1080#1094' '#1084#1077#1093#1072' (+10 '#1088#1091#1073')'
    TabOrder = 1
    OnClick = btnSell5FurClick
  end
  object BuyMeat: TButton
    Left = 224
    Top = 120
    Width = 209
    Height = 33
    Caption = #1050#1091#1087#1080#1090#1100' '#1086#1076#1085#1091' '#1077#1076#1080#1085#1080#1094#1091' '#1084#1103#1089#1072' (-10 '#1088#1091#1073')'
    TabOrder = 2
    OnClick = BuyMeatClick
  end
  object Buy5Meat: TButton
    Left = 224
    Top = 160
    Width = 209
    Height = 33
    Caption = #1050#1091#1087#1080#1090#1100' 5 '#1077#1076#1080#1085#1080#1094' '#1084#1103#1089#1072' (-50 '#1088#1091#1073')'
    TabOrder = 3
    OnClick = Buy5MeatClick
  end
  object btnBuyWheat: TButton
    Left = 224
    Top = 40
    Width = 209
    Height = 33
    Caption = #1050#1091#1087#1080#1090#1100' '#1086#1076#1085#1091' '#1077#1076#1080#1085#1080#1094#1091' '#1087#1096#1077#1085#1080#1094#1099' (-10 '#1088#1091#1073')'
    TabOrder = 4
    OnClick = btnBuyWheatClick
  end
  object btnBuy5Wheat: TButton
    Left = 224
    Top = 80
    Width = 209
    Height = 33
    Caption = #1050#1091#1087#1080#1090#1100' 5 '#1077#1076#1080#1085#1080#1094' '#1087#1096#1077#1085#1080#1094#1099' (-50 '#1088#1091#1073')'
    TabOrder = 5
    OnClick = btnBuy5WheatClick
  end
  object btnSellWheat: TButton
    Left = 8
    Top = 120
    Width = 209
    Height = 33
    Caption = #1055#1088#1086#1076#1072#1090#1100' '#1086#1076#1085#1091' '#1077#1076#1080#1085#1080#1094#1091' '#1087#1096#1077#1085#1080#1094#1099' (+5 '#1088#1091#1073')'
    TabOrder = 6
    OnClick = btnSellWheatClick
  end
  object btnSell5Wheat: TButton
    Left = 8
    Top = 160
    Width = 209
    Height = 33
    Caption = #1055#1088#1086#1076#1072#1090#1100' 5 '#1077#1076#1080#1085#1080#1094' '#1087#1096#1077#1085#1080#1094#1099' (+25 '#1088#1091#1073')'
    TabOrder = 7
    OnClick = btnSell5WheatClick
  end
  object btnSellMeat: TButton
    Left = 8
    Top = 200
    Width = 209
    Height = 33
    Caption = #1055#1088#1086#1076#1072#1090#1100' '#1086#1076#1085#1091' '#1077#1076#1080#1085#1080#1094#1091' '#1084#1103#1089#1072' (+5 '#1088#1091#1073')'
    TabOrder = 8
    OnClick = btnSellMeatClick
  end
  object btnSell5Meat: TButton
    Left = 8
    Top = 240
    Width = 209
    Height = 33
    Caption = #1055#1088#1086#1076#1072#1090#1100' 5 '#1077#1076#1080#1085#1080#1094' '#1084#1103#1089#1072' (+25 '#1088#1091#1073')'
    TabOrder = 9
    OnClick = btnSell5MeatClick
  end
  object btnForesterBuy: TButton
    Left = 8
    Top = 312
    Width = 209
    Height = 33
    Caption = #1051#1077#1089#1085#1080#1095#1080#1081' (-750 '#1088#1091#1073')'
    TabOrder = 10
    OnClick = btnForesterBuyClick
  end
  object btnSawmilBuy: TButton
    Left = 224
    Top = 312
    Width = 209
    Height = 33
    Caption = #1051#1077#1089#1086#1087#1080#1083#1082#1072' (-1000 '#1088#1091#1073')'
    TabOrder = 11
    OnClick = btnSawmilBuyClick
  end
  object btnCarpenterBuy: TButton
    Left = 8
    Top = 352
    Width = 209
    Height = 33
    Caption = #1055#1083#1086#1090#1085#1080#1082' (-1750 '#1088#1091#1073')'
    TabOrder = 12
    OnClick = btnCarpenterBuyClick
  end
  object btnQuarryBuy: TButton
    Left = 224
    Top = 352
    Width = 209
    Height = 33
    Caption = #1050#1072#1084#1077#1085#1086#1083#1086#1084#1085#1103' (-1000 '#1088#1091#1073')'
    TabOrder = 13
    OnClick = btnQuarryBuyClick
  end
  object btnMineBuy: TButton
    Left = 8
    Top = 392
    Width = 209
    Height = 33
    Caption = #1064#1072#1093#1090#1072' (-1500 '#1088#1091#1073')'
    TabOrder = 14
    OnClick = btnMineBuyClick
  end
  object btnBlacksmithBuy: TButton
    Left = 224
    Top = 392
    Width = 209
    Height = 33
    Caption = #1050#1091#1079#1085#1077#1094' (-2000 '#1088#1091#1073')'
    TabOrder = 15
    OnClick = btnBlacksmithBuyClick
  end
end
