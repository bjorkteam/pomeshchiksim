unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, StdCtrls, XPMan;

type
  TForm1 = class(TForm)
    PlayerName: TLabel;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lblMoney: TLabel;
    Main: TTimer;
    Date: TProgressBar;
    lblPeople: TLabel;
    lblMeat: TLabel;
    lblWheat: TLabel;
    DateNow: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lblHouses: TLabel;
    lblFarms: TLabel;
    lblMeatFarms: TLabel;
    VillageName: TLabel;
    BuildHouse: TButton;
    BuildFarm: TButton;
    BuildMeatFarm: TButton;
    LabelStaticName: TLabel;
    LabelStaticVillage: TLabel;
    lblFur: TLabel;                  //��� ����� ��� �� ��� ���������, �����.
    lblFurCount: TLabel;
    BuildHunterCabin: TButton;
    lblHunterCabin: TLabel;
    lblHunterCabinCount: TLabel;
    ToTheCity: TButton;
    Save: TButton;
    toVillage: TButton;
    toCity: TButton;
    grp1: TGroupBox;
    lblforester: TLabel;
    lblsawmill: TLabel;
    lblcarpenter: TLabel;
    lblquarry: TLabel;
    lblmine: TLabel;
    lblblacksmith: TLabel;
    grp2: TGroupBox;
    lbllogscount: TLabel;
    lblboardscount: TLabel;
    lblfurniturecount: TLabel;
    lblstonecount: TLabel;
    lblorecount: TLabel;
    lblmetalcount: TLabel;             //��� ����� ��� �� ��� ���������, �����.
    procedure MainTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BuildHouseClick(Sender: TObject);
    procedure BuildFarmClick(Sender: TObject);
    procedure BuildMeatFarmClick(Sender: TObject);
    procedure BuildHunterCabinClick(Sender: TObject);
    procedure ToTheCityClick(Sender: TObject);
    procedure SaveClick(Sender: TObject);
    procedure Start(h: integer);
    procedure toVillageClick(Sender: TObject);
    procedure toCityClick(Sender: TObject);
    procedure everyYear;
    procedure everyMonth;
  private
    { Private declarations }
  public
    { Public declarations }

  end;

  type vars=record
      PomName: string[255];
      VilName:string[255];
      VillageLevel:integer; //������� ���������(�����, �������, �����)
      days: integer;        //���(�� ������������)
      month: integer;       //������ � �������� �������
      monthStr: string[30];         //������ �������
      year: integer;     //���
      money: Integer;    //���������� �����, ��� ������ - 2000 ��
      people: integer;      //���������� ���������, ��� ������ - 5 �����
      meat: integer;        //���������� ����, ��� ������ - 5 ��. ����
      wheat: integer;       //���������� �������, ��� ������ - 5 ��. �������
      houses: Integer;      //���������� ���-�� �����, ��� ������ - 5 �����
      farms: integer;       //���������� ���-�� ����(�������), ��� ������ - 1 �����
      meatfarms: Integer;   //���������� ���-�� �����������(����), ��� ������ - 1 �����
      fur: Integer;         //���������� ���-�� ����(����� ��������� � �����/����), ��� ������ - 0
      huntercabin: Integer;  //���������� ���-�� ���������� �����, ��� ������ - 0
      forester: integer;     //������ ���
      sawmill: integer;
      carpenter: integer;
      quarry: integer;
      mine: integer;
      blacksmith: integer;
      logs:Integer;          //����� ���)
      boards:Integer;        //�����
      furniture:Integer;     //������
      stone:Integer;         //guess what?
      ore:Integer;           //����
      deathmetal:Integer;    // ������������������������
       th, tf, tmf, thc:integer;  //"����� ��������������" - ���������� ��� ���������
      max_houses:Integer;   //���� ����������� �� ��������� �����
      max_farms:Integer;
      max_meatfarms:Integer;
      max_huntercabin:Integer;
    end;
var
  Form1: TForm1;
  F:file of vars;
  forsave: string;
  time:integer;             //���������� ��� �������
  M:vars;
  
implementation

uses Unit3, NewGame;

{$R *.dfm}


procedure TForm1.everyYear;                            //��������� ���������� ������ 10 ������
begin
  M.money:=M.money+(M.people*5);                //������� � ������ ����� �� 5 ������
  M.meat:=M.meat+(M.meatfarms*5)-M.people+M.huntercabin;        //������� ���� = ����+(������ ����� ������������ �� 5 ����)-���������(������ ����� �� 1 ���� �������)+���� ������� ���� � ���� ��������
  M.wheat:=M.wheat+(M.farms*5)-M.people;          //������� �������(��.����)
  M.fur:=M.fur+M.huntercabin;               //������� ���� (�� ������� ��)
  if M.people<>M.houses then M.people:=M.people+1;
  if M.th=1 then
  begin
    M.houses:=M.houses+1;
    M.th:=0;
    BuildHouse.Enabled:=true;
  end;
  if M.tf=1 then
  begin
    M.farms:=M.farms+1;
    M.tf:=0;
    BuildFarm.Enabled:=True;
  end;
  if M.tmf=1 then
  begin
    M.meatfarms:=M.meatfarms+1;
    M.tmf:=0;
    BuildMeatFarm.Enabled:=True;
  end;
  if M.thc=1 then
  begin
    M.huntercabin:=M.huntercabin+1;
    M.thc:=0;
    BuildHunterCabin.Enabled:=True;
  end;

  M.month:=M.month+1;                         //����� ��������� �� 1
  if M.month=13 then                        //���� ����� 13(�_�) ��� �� ����� �����
  begin
    M.month:=1;                           //�������� ����� �� 1(������, �����)
    M.year:=M.year+1;                       //��������� 1 ���
  end;

  time:=0;                                //�������� ���������� �������
end;

procedure TForm1.everyMonth;
begin
    Date.Position:=time;                      //������� ��������-����

    case M.VillageLevel of
      1:begin
         LabelStaticVillage.Caption:='�����:';
         M.max_houses:=25;
         M.max_farms:=25;
         M.max_meatfarms:=25;
         M.max_huntercabin:=10;
        end;
      2:begin
         LabelStaticVillage.Caption:='�������:';
         M.max_houses:=80;
         M.max_farms:=80;
         M.max_meatfarms:=80;
         M.max_huntercabin:=30;
        end;
      3:begin
         LabelStaticVillage.Caption:='�����:';
         M.max_houses:=150;
         M.max_farms:=150;
         M.max_meatfarms:=150;
         M.max_huntercabin:=50;
        end
    end;

    if (M.people=25) and (M.VillageLevel=1) then toVillage.Visible:=True;
    if (M.people=80) and (M.VillageLevel=2) then toCity.Visible:=true;
    if M.houses=M.max_houses then BuildHouse.Enabled:=False;
    if M.farms=M.max_farms then BuildFarm.Enabled:=False;
    if M.meatfarms=M.max_meatfarms then BuildMeatFarm.Enabled:=False;
    if M.huntercabin=M.max_huntercabin then BuildHunterCabin.Enabled:=False;

    //������� �� switch
    case M.month of
    1:M.monthStr:='������';
    2:M.monthStr:='�������';
    3:M.monthStr:='����';
    4:M.monthStr:='������';
    5:M.monthStr:='���';
    6:M.monthStr:='����';
    7:M.monthStr:='����';
    8:M.monthStr:='������';
    9:M.monthStr:='��������';
    10:M.monthStr:='�������';
    11:M.monthStr:='������';
    12:M.monthStr:='�������';
    end;

    //������ �������� ����-���������� �������� (��������)
    if M.forester=1 then lblforester.Enabled:=True;
    if M.forester=1 then lbllogscount.Enabled:=True;
    if M.sawmill=1 then lblsawmill.Enabled:=True;
    if M.carpenter=1 then lblcarpenter.Enabled:=True;
    if M.quarry=1 then lblquarry.Enabled:=True;
    if M.mine=1 then lblmine.Enabled:=True;
    if M.blacksmith=1 then lblblacksmith.Enabled:=True;

    //������ ���������� �������� ��� ����-���������� �������� (��������)
    if M.forester=1 then lblforester.Enabled:=True;
    if M.sawmill=1 then lblsawmill.Enabled:=True;
    if M.carpenter=1 then lblcarpenter.Enabled:=True;
    if M.quarry=1 then lblquarry.Enabled:=True;
    if M.mine=1 then lblmine.Enabled:=True;
    if M.blacksmith=1 then lblblacksmith.Enabled:=True;


    //����������� ������
    lblMoney.Caption:=IntToStr(M.money)+' ������';
    lblPeople.Caption:=IntToStr(M.people)+' �����';
    lblMeat.Caption:=IntToStr(M.meat)+' ��.';
    lblWheat.Caption:=IntToStr(M.wheat)+' ��.';
    lblFurCount.Caption:=IntToStr(M.fur)+' ��.';
    DateNow.Caption:=M.monthStr+' '+IntToStr(M.year)+' �.';
    lblHouses.Caption:=IntToStr(M.houses);
    lblFarms.Caption:=IntToStr(M.farms);
    lblMeatFarms.Caption:=IntToStr(M.meatfarms);
    lblHunterCabinCount.Caption:=IntToStr(M.huntercabin);
end;

procedure TForm1.MainTimer(Sender: TObject);  //������ ����������� ������ �������
begin
  time:=time+10;                              //���������� � time �� 10
  Form1.PlayerName.Caption:= M.PomName;
  Form1.VillageName.Caption:= M.VilName;
  if time=100 then everyYear;                       //���� time ����� 100(������ ��� � 10 ������)

    //������ ��, ��� ����������� ������ �������
    everyMonth;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Application.Terminate;                        //��� �������� ���� - ������� ���������
end;

procedure TForm1.BuildHouseClick(Sender: TObject);
begin
  if M.money>100 then
  begin
    M.money:=M.money-100;
    M.th:=1;
    BuildHouse.Enabled:=false;
  end;
end;

procedure TForm1.BuildFarmClick(Sender: TObject);
begin
  if M.money>200 then
  begin
    M.money:=M.money-200;
    M.tf:=1;
    BuildFarm.Enabled:=false;
  end;
end;

procedure TForm1.BuildMeatFarmClick(Sender: TObject);
begin
  if M.money>200 then
  begin
    M.money:=M.money-200;
    M.tmf:=1;
    BuildMeatFarm.Enabled:=False;
  end;
end;


procedure TForm1.BuildHunterCabinClick(Sender: TObject);
begin
  if M.money>500 then
  begin
    M.money:=M.money-500;
    M.thc:=1;
    BuildHunterCabin.Enabled:=false;
  end;
end;

procedure TForm1.ToTheCityClick(Sender: TObject);
begin
  Form3.Show;
  Form3.left:=Form1.Left+Form1.Width+5;
  Form3.Top:=Form1.Top;
end;

procedure TForm1.SaveClick(Sender: TObject);
begin
  AssignFile(F, 'file.txt');
  Rewrite(F);
  Write(F, M);
  Closefile(F);
end;

procedure standart;
begin
  M.VillageLevel:=1;
  M.days:=1;
  M.month:=9;
  M.year:=1830;
  M.money:=2000;
  M.people:=5;
  M.meat:=5;
  M.wheat:=5;
  M.houses:=5;
  M.farms:=1;
  M.meatfarms:=1;
  M.fur:=0;
  M.huntercabin:=0;
end;

procedure loadVars;
begin
  AssignFile(F, 'file.txt');
  if FileExists('file.txt') then
  begin
  Reset(F);
  read(F, M);
  CloseFile(F);
  Form1.PlayerName.Caption:= M.PomName;
  Form1.VillageName.Caption:= M.VilName;
  end
  else
  standart;
end;

procedure TForm1.Start(h:integer);
begin
  case h of
  1:loadVars;
  0:standart;
  end;
end;


procedure TForm1.toVillageClick(Sender: TObject);
begin
  M.VillageLevel:=2;
  toVillage.Visible:=false;
end;

procedure TForm1.toCityClick(Sender: TObject);
begin
  M.VillageLevel:=3;
  toCity.Visible:=false;
end;

end.
