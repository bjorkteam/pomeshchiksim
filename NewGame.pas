unit NewGame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm2 = class(TForm)
    lbl1: TLabel;
    Name: TEdit;
    lbl2: TLabel;
    VilName: TEdit;
    Start: TButton;
    LoadSave: TButton;
    procedure StartClick(Sender: TObject);
    procedure LoadSaveClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;
  F: TextFile;
  fromsave: string;
  data:TStringList;

implementation
uses main;
{$R *.dfm}

procedure SetAsMainForm(aForm:TForm); //��������� �������������� ������� �����(�� �������)
var
  P:Pointer;
begin
  P := @Application.Mainform;
  Pointer(P^) := aForm;
end;

procedure TForm2.StartClick(Sender: TObject);  //����� ������� ������
begin
  Application.CreateForm(TForm1, Form1);       //������� �����
  SetAsMainForm(Form1);                        //��������� �� �������
  M.PomName:=Name.Text;         //�������� ��� ������
  M.VilName:=VilName.Text;     //�������� ��� �������
  Form1.Start(0);
  Form1.Show;                                  //�������� �����1
  Form2.Close();                               //������� �����2
end;



procedure TForm2.LoadSaveClick(Sender: TObject);
begin
  Application.CreateForm(TForm1, Form1);       //������� �����
  SetAsMainForm(Form1);                        //��������� �� �������
  Form1.Start(1);
  Form1.Show;                                  //�������� �����1
  Form2.Close();                               //������� �����2
end;

end.
