object Form1: TForm1
  Left = 185
  Top = 126
  Width = 870
  Height = 527
  Caption = #1057#1080#1084#1091#1083#1103#1090#1086#1088' '#1055#1086#1084#1077#1097#1080#1082#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PlayerName: TLabel
    Left = 120
    Top = 8
    Width = 111
    Height = 25
    Caption = 'PlayerName'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lbl1: TLabel
    Left = 72
    Top = 66
    Width = 70
    Height = 23
    Caption = #1044#1077#1085#1100#1075#1080':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lbl2: TLabel
    Left = 40
    Top = 90
    Width = 102
    Height = 23
    Caption = #1053#1072#1089#1077#1083#1077#1085#1080#1077':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lbl3: TLabel
    Left = 91
    Top = 114
    Width = 51
    Height = 23
    Caption = #1052#1103#1089#1086':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lbl4: TLabel
    Left = 54
    Top = 138
    Width = 88
    Height = 23
    Caption = #1055#1096#1077#1085#1080#1094#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblMoney: TLabel
    Left = 150
    Top = 66
    Width = 6
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblPeople: TLabel
    Left = 150
    Top = 90
    Width = 6
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblMeat: TLabel
    Left = 150
    Top = 114
    Width = 6
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblWheat: TLabel
    Left = 150
    Top = 138
    Width = 6
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object DateNow: TLabel
    Left = 272
    Top = 456
    Width = 97
    Height = 29
    Caption = 'DateNow'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lbl5: TLabel
    Left = 94
    Top = 250
    Width = 62
    Height = 23
    Caption = #1055#1086#1083#1077#1081':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lbl6: TLabel
    Left = 24
    Top = 274
    Width = 132
    Height = 23
    Caption = #1046#1080#1074#1086#1090#1085#1086#1092#1077#1088#1084':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lbl7: TLabel
    Left = 94
    Top = 226
    Width = 62
    Height = 23
    Caption = #1044#1086#1084#1086#1074':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblHouses: TLabel
    Left = 166
    Top = 226
    Width = 6
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblFarms: TLabel
    Left = 166
    Top = 250
    Width = 6
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblMeatFarms: TLabel
    Left = 166
    Top = 274
    Width = 6
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object VillageName: TLabel
    Left = 368
    Top = 8
    Width = 116
    Height = 25
    Caption = 'VillageName'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object LabelStaticName: TLabel
    Left = 16
    Top = 8
    Width = 101
    Height = 25
    Caption = #1042#1072#1096#1077' '#1080#1084#1103':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object LabelStaticVillage: TLabel
    Left = 272
    Top = 8
    Width = 89
    Height = 25
    Caption = #1044#1077#1088#1077#1074#1085#1103':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblFur: TLabel
    Left = 98
    Top = 160
    Width = 41
    Height = 23
    Caption = #1052#1077#1093':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblFurCount: TLabel
    Left = 150
    Top = 160
    Width = 6
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblHunterCabin: TLabel
    Left = 9
    Top = 296
    Width = 147
    Height = 23
    Caption = #1044#1086#1084#1086#1074' '#1086#1093#1086#1090#1085#1080#1082#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblHunterCabinCount: TLabel
    Left = 166
    Top = 296
    Width = 6
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Date: TProgressBar
    Left = 488
    Top = 464
    Width = 200
    Height = 20
    Step = 1
    TabOrder = 0
  end
  object BuildHouse: TButton
    Left = 8
    Top = 384
    Width = 217
    Height = 25
    Caption = #1055#1086#1089#1090#1088#1086#1080#1090#1100' '#1076#1086#1084' (100 '#1088#1091#1073')'
    TabOrder = 1
    OnClick = BuildHouseClick
  end
  object BuildFarm: TButton
    Left = 8
    Top = 408
    Width = 217
    Height = 25
    Caption = #1042#1089#1087#1072#1093#1072#1090#1100' '#1087#1086#1083#1077' (200 '#1088#1091#1073')'
    TabOrder = 2
    OnClick = BuildFarmClick
  end
  object BuildMeatFarm: TButton
    Left = 8
    Top = 432
    Width = 217
    Height = 25
    Caption = ' '#1055#1086#1089#1090#1088#1086#1080#1090#1100' '#1078#1080#1074#1086#1090#1085#1086#1092#1077#1088#1084#1091' (200 '#1088#1091#1073')'
    TabOrder = 3
    OnClick = BuildMeatFarmClick
  end
  object BuildHunterCabin: TButton
    Left = 8
    Top = 456
    Width = 217
    Height = 25
    Caption = #1055#1086#1089#1090#1088#1086#1080#1090#1100' '#1076#1086#1084' '#1086#1093#1086#1090#1085#1080#1082#1072' (500 '#1088#1091#1073')'
    TabOrder = 4
    OnClick = BuildHunterCabinClick
  end
  object ToTheCity: TButton
    Left = 560
    Top = 416
    Width = 129
    Height = 33
    Caption = #1042' '#1043#1086#1088#1086#1076'!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = ToTheCityClick
  end
  object Save: TButton
    Left = 608
    Top = 8
    Width = 75
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 6
    OnClick = SaveClick
  end
  object toVillage: TButton
    Left = 8
    Top = 352
    Width = 217
    Height = 25
    Caption = #1059#1083#1091#1095#1096#1080#1090#1100' '#1076#1086' '#1076#1077#1088#1077#1074#1085#1080'!'
    TabOrder = 7
    Visible = False
    OnClick = toVillageClick
  end
  object toCity: TButton
    Left = 8
    Top = 352
    Width = 217
    Height = 25
    Caption = #1059#1083#1091#1095#1096#1080#1090#1100' '#1076#1086' '#1075#1086#1088#1086#1076#1072'!'
    TabOrder = 8
    Visible = False
    OnClick = toCityClick
  end
  object grp1: TGroupBox
    Left = 704
    Top = 8
    Width = 145
    Height = 113
    Caption = #1059#1085#1080#1082#1072#1083#1100#1085#1099#1077' '#1087#1086#1089#1090#1088#1086#1081#1082#1080
    TabOrder = 9
    object lblforester: TLabel
      Left = 8
      Top = 16
      Width = 48
      Height = 13
      Caption = #1051#1077#1089#1085#1080#1095#1080#1081
      Enabled = False
    end
    object lblsawmill: TLabel
      Left = 8
      Top = 32
      Width = 54
      Height = 13
      Caption = #1051#1077#1089#1086#1087#1080#1083#1082#1072
      Enabled = False
    end
    object lblcarpenter: TLabel
      Left = 8
      Top = 48
      Width = 43
      Height = 13
      Caption = #1055#1083#1086#1090#1085#1080#1082
      Enabled = False
    end
    object lblquarry: TLabel
      Left = 8
      Top = 64
      Width = 67
      Height = 13
      Caption = #1050#1072#1084#1077#1085#1086#1083#1086#1084#1085#1103
      Enabled = False
    end
    object lblmine: TLabel
      Left = 8
      Top = 80
      Width = 34
      Height = 13
      Caption = #1064#1072#1093#1090#1072
      Enabled = False
    end
    object lblblacksmith: TLabel
      Left = 8
      Top = 96
      Width = 36
      Height = 13
      Caption = #1050#1091#1079#1085#1077#1094
      Enabled = False
    end
  end
  object grp2: TGroupBox
    Left = 704
    Top = 128
    Width = 145
    Height = 121
    Caption = #1059#1085#1080#1082#1072#1083#1100#1085#1099#1077' '#1088#1077#1089#1091#1088#1089#1099
    TabOrder = 10
    object lbllogscount: TLabel
      Left = 8
      Top = 16
      Width = 40
      Height = 13
      Caption = #1041#1088#1105#1074#1085#1072':'
      Enabled = False
    end
    object lblboardscount: TLabel
      Left = 8
      Top = 32
      Width = 35
      Height = 13
      Caption = #1044#1086#1089#1082#1080':'
      Enabled = False
    end
    object lblfurniturecount: TLabel
      Left = 8
      Top = 48
      Width = 42
      Height = 13
      Caption = #1052#1077#1073#1077#1083#1100':'
      Enabled = False
    end
    object lblstonecount: TLabel
      Left = 8
      Top = 64
      Width = 41
      Height = 13
      Caption = #1050#1072#1084#1077#1085#1100':'
      Enabled = False
    end
    object lblorecount: TLabel
      Left = 8
      Top = 80
      Width = 29
      Height = 13
      Caption = #1056#1091#1076#1072':'
      Enabled = False
    end
    object lblmetalcount: TLabel
      Left = 8
      Top = 96
      Width = 40
      Height = 13
      Hint = #1050#1072#1082' '#1084#1077#1090#1072#1083', '#1090#1086#1083#1100#1082#1086' '#1078#1105#1089#1090#1095#1077
      Caption = #1052#1048#1058#1054#1051':'
      Enabled = False
    end
  end
  object Main: TTimer
    OnTimer = MainTimer
    Left = 624
    Top = 32
  end
end
