unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm3 = class(TForm)
    lblMarketName: TLabel;
    btnSellFur: TButton;
    btnSell5Fur: TButton;
    BuyMeat: TButton;
    Buy5Meat: TButton;
    btnBuyWheat: TButton;
    btnBuy5Wheat: TButton;
    btnSellWheat: TButton;
    btnSell5Wheat: TButton;
    btnSellMeat: TButton;
    btnSell5Meat: TButton;
    lblUniqueBuildings: TLabel;
    btnForesterBuy: TButton;
    btnSawmilBuy: TButton;
    btnCarpenterBuy: TButton;
    btnQuarryBuy: TButton;
    btnMineBuy: TButton;
    btnBlacksmithBuy: TButton;
    procedure btnSellFurClick(Sender: TObject);
    procedure btnSell5FurClick(Sender: TObject);
    procedure BuyMeatClick(Sender: TObject);
    procedure Buy5MeatClick(Sender: TObject);
    procedure btnBuyWheatClick(Sender: TObject);
    procedure btnBuy5WheatClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSellWheatClick(Sender: TObject);
    procedure btnSell5WheatClick(Sender: TObject);
    procedure btnSellMeatClick(Sender: TObject);
    procedure btnSell5MeatClick(Sender: TObject);
    procedure btnForesterBuyClick(Sender: TObject);
    procedure btnSawmilBuyClick(Sender: TObject);
    procedure btnCarpenterBuyClick(Sender: TObject);
    procedure btnQuarryBuyClick(Sender: TObject);
    procedure btnMineBuyClick(Sender: TObject);
    procedure btnBlacksmithBuyClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses main;

{$R *.dfm}


procedure TForm3.btnSellFurClick(Sender: TObject);   //��������������� ���� ���� ��
begin
  if M.fur=0 then
  begin       //����������� ����, �������� ���� �� � ������ *fur*
    M.fur:=M.fur;
    M.money:=M.money;
  end
  else
  begin
    M.fur:=M.fur-1;
    M.money:=M.money+2;
  end;
end;

procedure TForm3.btnSell5FurClick(Sender: TObject);
begin
  if (M.fur=0) or (M.fur<5) then
  begin       //����������� ����, �������� ���� �� � ������ *fur*
    M.fur:=M.fur;
    M.money:=M.money;
  end
  else
  begin
    M.fur:=M.fur-5;
    M.money:=M.money+10;
  end;
end;



procedure TForm3.BuyMeatClick(Sender: TObject);
begin
  if (M.money<=0) and (M.money<5) then
  begin       //����������� ����, �������� ���� �� � ������ *money*
    M.meat:=M.meat;
    M.money:=M.money;
  end
  else
  begin
    M.meat:=M.meat+1;
    M.money:=M.money-10;
  end;
end;


procedure TForm3.Buy5MeatClick(Sender: TObject);
begin
  if (M.money<=0) and (M.money<25) then
  begin       //����������� ����, �������� ���� �� � ������ *fur*
    M.meat:=M.meat;
    M.money:=M.money;
  end
  else
  begin
    M.meat:=M.meat+5;
    M.money:=M.money-50;
  end;
end;


procedure TForm3.btnBuyWheatClick(Sender: TObject);
begin
  if (M.money<=0) and (M.money<5) then
  begin       //����������� ����, �������� ���� �� � ������ *fur*
    M.wheat:=M.wheat;
    M.money:=M.money;
  end
  else
  begin
    M.wheat:=M.wheat+1;
    M.money:=M.money-10;
  end;
end;

procedure TForm3.btnBuy5WheatClick(Sender: TObject);
begin
  if (M.money<=0) and (M.money<25) then begin       //�� ����� � �� ���������... ���������... ������� �����
    M.wheat:=M.wheat;
    M.money:=M.money;
  end
  else
  begin
    M.wheat:=M.wheat+5;
    M.money:=M.money-50;
  end;
end;

procedure TForm3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Form3.Hide;
end;

procedure TForm3.btnSellWheatClick(Sender: TObject);
begin
  if M.wheat=0 then
  begin
    M.wheat:=M.wheat;
    M.money:=M.money;
  end
  else
  begin
    M.wheat:=M.wheat-1;
    M.money:=M.money+5;
  end;
end;

procedure TForm3.btnSell5WheatClick(Sender: TObject);
begin
  if (M.wheat=0) or (M.wheat<5) then
  begin
    M.wheat:=M.wheat;
    M.money:=M.money;
  end
  else
  begin
    M.wheat:=M.wheat-5;
    M.money:=M.money+25;
  end;
end;

procedure TForm3.btnSellMeatClick(Sender: TObject);
begin
  if M.meat=0 then
  begin
    M.meat:=M.meat;
    M.money:=M.money;
  end
  else
  begin
    M.meat:=M.meat-1;
    M.money:=M.money+5;
  end;
end;

procedure TForm3.btnSell5MeatClick(Sender: TObject);
begin
  if (M.meat=0) or (M.meat<5) then
  begin
    M.meat:=M.meat;
    M.money:=M.money;
  end
  else
  begin
    M.meat:=M.meat-5;
    M.money:=M.money+25;
  end;
end;

procedure TForm3.btnForesterBuyClick(Sender: TObject);
begin
   if (M.forester=0) and (M.money>750) then
   begin
     M.forester:=1;
     M.money:=M.money-750;
   end
   else
   begin
     M.forester:=M.forester;
     M.money:=M.money;
   end;
end;

procedure TForm3.btnSawmilBuyClick(Sender: TObject);
begin
   if (M.sawmill=0) and (M.money>1000) then
   begin
     M.sawmill:=1;
     M.money:=M.money-1000;
   end
   else
   begin
     M.sawmill:=M.sawmill;
     M.money:=M.money;
   end;
end;

procedure TForm3.btnCarpenterBuyClick(Sender: TObject);
begin
   if (M.carpenter=0) and (M.money>1750) then
   begin
     M.carpenter:=1;
     M.money:=M.money-1750;
   end
   else
   begin
     M.carpenter:=M.carpenter;
     M.money:=M.money;
   end;
end;

procedure TForm3.btnQuarryBuyClick(Sender: TObject);
begin
   if (M.quarry=0) and (M.money>1000) then
   begin
     M.quarry:=1;
     M.money:=M.money-1000;
   end
   else
   begin
     M.quarry:=M.quarry;
     M.money:=M.money;
   end;
end;

procedure TForm3.btnMineBuyClick(Sender: TObject);
begin
   if (M.mine=0) and (M.money>1500) then
   begin
     M.mine:=1;
     M.money:=M.money-1500;
   end
   else
   begin
     M.mine:=M.mine;
     M.money:=M.money;
   end;
end;

procedure TForm3.btnBlacksmithBuyClick(Sender: TObject);
begin
   if (M.blacksmith=0) and (M.money>2000) then
   begin
     M.blacksmith:=1;
     M.money:=M.money-2000;
   end
   else
   begin
     M.blacksmith:=M.blacksmith;
     M.money:=M.money;
   end;
end;

end.
